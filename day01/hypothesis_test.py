import numpy as np

def donotknowwhatziis(theta, x):
	w = []
	for j in range(len(x)):
		y = 0
		for i in range(len(theta)):
			y += np.dot(theta[i], x[j])
		w.append(y)
	return np.array(w)


X1 = np.array([[0.], [1.], [2.], [3.], [4.]])
theta1 = np.array([[2.], [4.]])
print(donotknowwhatziis(theta1, X1))
