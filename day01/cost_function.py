import numpy as np

def predict_(theta, x):
	w = []
	z = theta[0]
	for i in x:
		m = 0
		for y in range(len(theta)):
			m = np.dot(i, theta[1:])
		m += z
		w.append(m)
	return np.array(w)

def cost_elem_(theta, X, Y):
	w = predict_(theta, X)
	W = []
	for i in range(len(X)):
		W.append(1/(2*len(w))*(np.subtract(w[i], Y[i])**2))
	return np.array(W)

def cost_(theta, X, Y):
	w = cost_elem_(theta, X, Y)
	return np.sum(w)

X1 = np.array([[0.], [1.], [2.], [3.], [4.]])
theta1 = np.array([[2.], [4.]])
Y1 = np.array([[2.], [7.], [12.], [17.], [22.]])
X2 = np.array([[0.2, 2., 20.], [0.4, 4., 40.], [0.6, 6., 60.], [0.8, 8.,
80.]])
theta2 = np.array([[0.05], [1.], [1.], [1.]])
Y2 = np.array([[19.], [42.], [67.], [93.]])
print(cost_elem_(theta1, X1, Y1))
print(cost_(theta1, X1, Y1))
print(cost_elem_(theta2, X2, Y2))
print(cost_(theta2, X2, Y2))
