import numpy as np
from d0e4_tools import add_intercept

def simple_predict(x, theta):
    y = []

    if type(x) != list or type(theta) != list:
	return ("/!\ Examples (x) and variables (theta) should be stored as vectors in lists /!\\")

    else:
	for i in range(len(x) - 1):
	    y.append(float(theta[0] + theta[1] * x[i]))
	return y

def simple_predict_np(x, theta):
    yy = []

    if type(x) != np.ndarray or type(theta) != np.ndarray:
	return ("/!\ Examples (x) and variables (theta) should be stored as numpy.ndarray/!\\")
    
    else:
	for i in range(len(x) - 1):
	    yy.append(float(theta[0] + theta[1] * x[i]))
	y = np.array(yy)
	return y
    

def predict_(x, theta):
    if type(x) != np.ndarray or type(theta) != np.ndarray:
	return ("/!\ Examples (x) and variables (theta) should be stored as numpy.ndarray/!\\")

#MODIFY ERROR CASE > DIMENSIONS SHOULD BE (m * n) & (n + 1, 1) I THINK
#    elif len(np.shape(x)) != 1 or len(np.shape(theta)) != 1:
#	return ("/!\ Examples (x) and variables (theta) should be stored as numpy.ndarray of dimensions (m * 1) and (2 * 1) /!\\")

    else:
	yy = []
	X = add_intercept(x)
	for j in range(len(X)):
	    item = 0
	    for y in range (np.shape(X)[1]):
		item += X[j][y] * theta[y]
	    yy.append(item)
	yy = np.array(yy)
	return yy
