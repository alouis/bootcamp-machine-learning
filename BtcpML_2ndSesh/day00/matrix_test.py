from d0e1_matrix import Matrix

m1 = Matrix([[0, 2, 3], [1, 3, 5]])
#print(m1.shape)
#print(m1.values)
m2 = Matrix((2, 3))
#print(m2.shape)
#print(m2.values)
m3 = Matrix([[0.0, 1.0, 2.0], [1.0, 2.0, 3.0], [2.0, 3.0, 4.0]], (3, 3))
#print(m3.shape)
#print(m3.values)
m4 = Matrix((3, 3), [[0.0, 1.0, 2.0], [1.0, 2.0, 3.0], [2.0, 3.0, 4.0]])
#print(m4.shape)
#print(m4.values)
m6 = [[1, 2, 3], [1, 2, 3]]

m5 = 3 / m2
print(m5)
m7 = m6 - m1
print(m7)


print("\nMULTIPLYING MATRICES\n")
m8 = Matrix([[1, 2, 0], [-1, 2, 1]])
m12 = [[1, 2, 0], [-1, 2, 1]]
m9 = Matrix([[1, -1], [0, 2], [-1, -1]])
m10 = Matrix([[1, -2], [3, 4]])

print("result expected :")
print(m10)
print("results obtained :")
m11 = m12 * m9
print(m11)
m13 = m8 * m9
print(m13)
print("wrong results :")
m13 = m10 * m9
print(m13)
m13 = m8 * m12
print(m13)
