import numpy as np
from d0e4_tools import add_intercept


x = np.arange(1,6)
y = np.arange(1,10).reshape((3,3))

print ("x = ", x)
print add_intercept(x)
print ("y = ", y)
print add_intercept(y)
