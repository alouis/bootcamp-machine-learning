import numpy as np

def cost_elem_(y, y_hat):
#ERROR CASE SHOULD BE MODIFY FOR NP ARRAYS WITH 1 LINE ARE SHAPED (M,)
#    if np.shape(y) != np.shape(y_hat):
#	return("/!\ Predicted values (y_hat) and real values (y) should be stored as numpy arrays of same dimension (m * n) /!\\")

#    else:
	cost_elem = []
	for i in range(len(y)):
	    cost_elem.append(1.0 / (2.0 * float(len(y_hat)))*(np.subtract(y_hat[i], y[i])**2))
	return np.array(cost_elem)

def cost_(y, y_hat):
#ERROR CASE SHOULD BE MODIFY FOR NP ARRAYS WITH 1 COLUMNS ARE SHAPED (M,)
#    if np.shape(y) != np.shape(y_hat):
#	return("/!\ Predicted values (y_hat) and real values (y) should be stored as numpy arrays of same dimension (m * n) /!\\")

 #   else:
	cost_elem = cost_elem_(y, y_hat)
	cost = np.sum(cost_elem)
	return cost
    
#def vec_cost_(y, y_hat):
#SAME THAN COST_ BUT ALL AT ONCE USING MATRICES MULTIPLICATION OR DOT PRODUCT, NO FOR LOOP   
