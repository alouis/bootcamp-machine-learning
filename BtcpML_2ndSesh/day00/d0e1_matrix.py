from d0e0_vector import Vector

class Matrix:

    def __init__(self, *args):
	self.lines = 0
	self.columns = 0
	self.values = []
	self.shape = ()

	for j in args:
	    if isinstance(j, list):
		self.values = j
		lines = 0
		for x in j:
		    if type(x) == list:
			lines += 1
			columns = 0
			for y in x:
			  columns += 1
		    else:
			print("/!\ Matrix's data should be stored as a list of lists /!\\")
			return
		if self.shape == ():
		    self.lines = lines
		    self.columns = columns
		    self.shape = (lines, columns / lines)
		else:
		    if self.lines != lines:
			print("/!\ Number of lines (m) doesn't match /!\\")
			return
		    if self.columns != columns:
			print("/!\ Number of columns (n) doesn't match /!\\")
			return

	
	    if isinstance(j, tuple):
		if len(j) != 2:
		    print("/!\ Shape of the matrix should be (m, n) w/ m = nbr of lines and n = nbr of columns /!\\")
		    return
		self.shape = j
		self.lines = j[0]
		self.columns = j[1]
		if self.values == []:
		    z = 0
		    for x in range(j[0]):   
			line = []
			for y in range(j[1]):
			    line.append(z)
			    z += 1
			self.values.append(line)
		else:
		    if self.lines != len(self.values):
			print("/!\ Number of lines (m) doesn't match /!\\")
			return
		    for x in self.values:
			columns = 0
			for y in x:
			    columns += 1
			if self.columns != columns:
			    print("/!\ Number of columns (n) doesn't match /!\\")
			    return


    def __add__(self, other):
	add = []

	if type(other) == int or type(other) == float:
	    print("/!\ Adding scalar and matrix isn't possible /!\\")
	    return

	elif isinstance(other, Matrix):
	    if other.lines != self.lines:
		print("/!\ Matrices don't have the same nbr of lines /!\\")
		return

	    elif other.columns != self.columns:
		print("/!\ Matrices don't have the same nbr of colunms /!\\")
		return

	    else:
		for x in range(self.lines):
		    line = []
		    for y in range(self.columns):
			line.append(self.values[x][y] + other.values[x][y])
		    add.append(line)
		
	elif isinstance(other, Matrix) == False:
	    if type(other) != list:
		print("/!\ Matrix's data should be stored as a list of lists /!\\")
		return

	    else:
		lines = 0
		for x in other:
		    if type(x) != list:
			print("/!\ Matrix's data should be stored as a list of lists /!\\")
			return
		    else:
			lines +=1
			columns = 0
			for y in x:
			    columns +=1
			if columns != self.columns:
			    print("/!\ Matrices don't have the same nbr of colunms /!\\")
			    return

		if lines != self.lines:
		    print("/!\ Matrices don't have the same nbr of lines /!\\")
		    return

		else:
		    for x in range(self.lines):
			line = []
			for y in range(self.columns):
			    line.append(self.values[x][y] + other[x][y])
			add.append(line)

	return Matrix(add)


    def __radd__(self, other):
	radd = []

	if type(other) == int or type(other) == float:
	    print("/!\ Adding scalar and matrix isn't possible /!\\")
	    return
	
	elif isinstance(other, Matrix) == False:
	    if type(other) != list:
		print("/!\ Matrix's data should be stored as a list of lists /!\\")
		return

	    else:
		lines = 0
		for x in other:
		    if type(x) != list:
			print("/!\ Matrix's data should be stored as a list of lists /!\\")
			return
		    else:
			lines +=1
			columns = 0
			for y in x:
			    columns +=1
			if columns != self.columns:
			    print("/!\ Matrices don't have the same nbr of colunms /!\\")
			    return

		if lines != self.lines:
		    print("/!\ Matrices don't have the same nbr of lines /!\\")
		    return

		else:
		    for x in range(self.lines):
			line = []
			for y in range(self.columns):
			    line.append(self.values[x][y] + other[x][y])
			radd.append(line)

	return Matrix(radd)


    def __sub__(self, other):
	sub = []

	if type(other) == int or type(other) == float:
	    print("/!\ Substracting scalar and matrix isn't possible /!\\")
	    return

	elif isinstance(other, Matrix):
	    if other.lines != self.lines:
		print("/!\ Matrices don't have the same nbr of lines /!\\")
		return

	    elif other.columns != self.columns:
		print("/!\ Matrices don't have the same nbr of colunms /!\\")
		return

	    else:
		for x in range(self.lines):
		    line = []
		    for y in range(self.columns):
			line.append(self.values[x][y] - other.values[x][y])
		    sub.append(line)

	elif isinstance(other, Matrix) == False:
	    if type(other) != list:
		print("/!\ Matrix's data should be stored as a list of lists /!\\")
		return

	    else:
		lines = 0
		for x in other:
		    if type(x) != list:
			print("/!\ Matrix's data should be stored as a list of lists /!\\")
			return
		    else:
			lines +=1
			columns = 0
			for y in x:
			    columns +=1
			if columns != self.columns:
			    print("/!\ Matrices don't have the same nbr of colunms /!\\")
			    return

		if lines != self.lines:
		    print("/!\ Matrices don't have the same nbr of lines /!\\")
		    return

		else:
		    for x in range(self.lines):
			line = []
			for y in range(self.columns):
			    line.append(self.values[x][y] - other[x][y])
			sub.append(line)

	return Matrix(sub)


    def __rsub__(self, other):
	rsub = []

	if type(other) == int or type(other) == float:
	    print("/!\ Adding scalar and matrix isn't possible /!\\")
	    return
	
	elif isinstance(other, Matrix) == False:
	    if type(other) != list:
		print("/!\ Matrix's data should be stored as a list of lists /!\\")
		return

	    else:
		lines = 0
		for x in other:
		    if type(x) != list:
			print("/!\ Matrix's data should be stored as a list of lists /!\\")
			return
		    else:
			lines +=1
			columns = 0
			for y in x:
			    columns +=1
			if columns != self.columns:
			    print("/!\ Matrices don't have the same nbr of colunms /!\\")
			    return

		if lines != self.lines:
		    print("/!\ Matrices don't have the same nbr of lines /!\\")
		    return

		else:
		    for x in range(self.lines):
			line = []
			for y in range(self.columns):
			    line.append(other[x][y] - self.values[x][y])
			rsub.append(line)
	    
	    return Matrix(rsub)

    def __div__(self, other):
	div = []

	if type(other) != int:
	    print("/!\ Only divisions between a matrix (m * n) and a scalar (1 * 1) are possible /!\\")
	    return

	else:
	    for x in range(self.lines):
		line = []
		for y in range(self.columns):
		    line.append(y / other)
		div.append(line)
	
	return Matrix(div)


    def __rdiv__(self, other):
	rdiv = []

	if type(other) != int:
	    print("/!\ Only divisions between a matrix (m * n) and a scalar (1 * 1) are possible /!\\")
	    return

	else:
	    for x in range(self.lines):
		line = []
		for y in range(self.columns):
		    line.append(y / other)
		rdiv.append(line)
	
	print("/!\ Can't divide scalar by matrix, result is the operation the other way around (matrix / scalar) /!\\")
	return Matrix(rdiv)


    def __mul__(self, other):
	mul = []

	if type(other) == int:
	    for x in range(self.lines):
		line = []
		for y in range(self.columns):
		    line.append(y * other)
		mul.append(line)

	elif isinstance(other, Matrix):
	    if self.columns != other.lines:
		print("/!\ Mutiplication between two matrices requires matrices of compatible dimensions: (m * n) and (n * p) in that order /!\\")
		return

	    else:
		for i in range(other.columns):
		    line = []
		    for x in range(self.lines):
			item = 0
			for y in range(self.columns):
			    item += self.values[x][y] * other.values[y][i]
			line.append(item)
		    mul.append(line)	

	elif isinstance(other, Matrix) == False:
	    if type(other) != list:
		print("/!\ Matrix's data should be stored as a list of lists /!\\")
		return

	    else:
		lines = 0
		for x in other:
		    if type(x) != list:
			print("/!\ Matrix's data should be stored as a list of lists /!\\")
			return
		    else:
			lines +=1
			columns = 0
			for y in x:
			    columns +=1

		if lines != self.columns:
		    print("/!\ Matrices dimensions are not compatible, should be (m *n) and (n * p) in that order /!\\")
		    return
	    
		for i in range(columns):
		    line = []
		    for x in range(self.lines):
			item = 0
			for y in range(self.columns):
			    item += self.values[x][y] * other[y][i]
			line.append(item)
		    mul.append(line)

	return Matrix(mul)


    def __rmul__(self, other):
	rmul = []

	if type(other) == int:
	    for x in range(self.lines):
		line = []
		for y in range(self.columns):
		    line.append(y * other)
		rmul.append(line)

	elif isinstance(other, Matrix) == False:
	    if type(other) != list:
		print("/!\ Matrix's data should be stored as a list of lists /!\\")
		return

	    else:
		lines = 0
		for x in other:
		    if type(x) != list:
			print("/!\ Matrix's data should be stored as a list of lists /!\\")
			return
		    else:
			lines +=1
			columns = 0
			for y in x:
			    columns +=1

		if lines != self.columns:
		    print("/!\ Matrices dimensions are not compatible, should be (m *n) and (n * p) in that order /!\\")
		    return
	    
		for i in range(self.columns):
		    line = []
		    for x in range(lines):
			item = 0
			for y in range(columns):
			    item += other[x][y] * self.values[y][i]
			line.append(item)
		    rmul.append(line)

	return Matrix(rmul)


    def __str__(self):
	return(str(self.values))

    def __repr__(self):
	return(self.values)
