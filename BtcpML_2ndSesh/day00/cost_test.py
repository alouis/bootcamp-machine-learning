import numpy as np
from d0e7_cost import cost_elem_
from d0e7_cost import cost_
from d0e3e5_prediction import predict_

#examples
x1 = np.array([[0.], [1.], [2.], [3.], [4.]])
#variables
theta1 = np.array([[2.], [4.]])
#predicted  values
y_hat1 = predict_(x1, theta1)
print np.shape(y_hat1)
#real values
y1 = np.array([[2.], [7.], [12.], [17.], [22.]])
print np.shape(y1)

#For each value > average squared distances' values = element
print cost_elem_(y1, y_hat1)

#Value of cost function > average of each elements
print cost_(y1, y_hat1)


x2 = np.array([[0.2, 2., 20.], [0.4, 4., 40.], [0.6, 6., 60.], [0.8, 8., 80.]])
theta2 = np.array([[0.05], [1.], [1.], [1.]])
y_hat2 = predict_(x2, theta2)
y2 = np.array([[19.], [42.], [67.], [93.]])
print np.shape(y2)
print cost_elem_(y2, y_hat2)
print cost_(y2, y_hat2)

x3 = np.array([0, 15, -9, 7, 12, 3, -21])
print np.shape(x3)
theta3 = np.array([[0.], [1.]])
print np.shape(theta3)
y_hat3 = predict_(x3, theta3)
print np.shape(y_hat3)
y3 = np.array([2, 14, -13, 5, 12, 4, -19])
print np.shape(y3)
print cost_elem_(y3, y_hat3)
print cost_(y3, y_hat3)

print cost_(y3, y3)
