import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from d0e3e5_prediction import predict_
import scipy.stats


def plot(x, y, theta):
    if type(x) != np.ndarray or type(y) != np.ndarray or type(theta) != np.ndarray:
	return ("/!\ Examples (x,y) and variables (theta) should be stored as numpy.ndarray of dimensions (m * n) and (n + 1 * 1) /!\\")
    if len(x) != len(y):
	return ("/!\ Examples (x,y) should be stored as numpy.ndarray of dimensions (m * n)/!\\")
#    if np.shape(x)[1] != len(theta1):
#	return ("/!\ Variables (theta) should be stored as numpy.ndarray of dimensions (n + 1 * 1)/!\\")

    else:
	plt.scatter(x,y)
	plt.xlabel('x')
	plt.ylabel('y')
	plt.legend('prediction line', loc='lower left')
	predict = predict_(x, theta)
	plt.plot(x, predict, color='red')
#	plt.show()
	

#LEAST SQUARE METHOD - ALMOST WORKS
#Calculating means
	xmean = np.mean(x)
	ymean = np.mean(y)
#Calculating correlation 
	r = scipy.stats.pearsonr(x,y)[0]
#Calculating standard deviation
	xstd = np.std(x)
	ystd = np.std(y)
#Calculating slope of the line
	m = r * (ystd / xstd)
#Calculating y-intercept
	b = ymean - r * xmean

#Calculating line's ordinates ?
	yy = []
	for i in range(len(y)):
	    yy.append(m * y[i] + b)

#Calculating line's abscisses ?
	xx = []
	for i in range(len(x)):
	    xx.append(m * x[i] + b)
#Draw line
	plt.plot(x, xx, color='green')
	plt.plot(y, yy, color='blue')
	plt.show()


