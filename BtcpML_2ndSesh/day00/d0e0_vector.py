class Vector:

    def __init__(self, other):
	if type(other) == int:
	    self.size = other
	    self.value = []
	    for x in range(other + 1):
		self.value.append(float(x))
		x += 1

	elif type(other) == list:
	    self.size = len(other)
	    self.value = other

	elif type(other) == tuple:
	    self.value = []
	    x = min(other)
	    y = max(other)
	    while x != y + 1:
		self.value.append(float(x))
		x += 1
	    self.size = len(self.value)

	else:
	    print("/!\ Wrong type of class attribute /!\\")
	    return


    def __add__(self, other):
	add = []

	if type(other) == int or type(other) == float:
	    print("/!\ Adding scalar and vector is forbidden here /!\\")
	    return

	elif isinstance(other, Vector):
	    if self.size != other.size:
		print("/!\ Vectors have to be of same dimension (m * 1) /!\\")
		return
	    else:
		for x in range(self.size):
		    add.append(self.value[x] + other.value[x])

	elif isinstance(other, Vector) == False:
	    if self.size != len(other):
		print("/!\ Vectors have to be of same dimension (m * 1) /!\\")
		return
	    else:
		for x in range(self.size):
		    add.append(self.value[x] + other[x])

	return Vector(add)


    def __radd__(self, other):
	radd = []

	if isinstance(other, Vector) == False:
	    if len(other) != self.size:
		print("/!\ Vectors have to be of same dimension (m * 1) /!\\")
		return
	    else:
		for x in range(self.size):
		    radd.append(other[x] + self.value[x])

	return Vector(radd)


    def __sub__(self, other):
	sub = []

	if type(other) == int or type(other) == float:
	    print("/!\ Substracting scalar and vector is forbidden here /!\\")
	    return


	elif isinstance(other, Vector):
	    if self.size != other.size:
		print("/!\ Vectors have to be of same dimension (m * 1) /!\\")
		return
	    else:
		for x in range(self.size):
		    sub.append(self.value[x] - other.value[x])

	elif isinstance(other, Vector) == False:
	    if len(other) != self.size:
		print("/!\ Vectors have to be of same dimension (m * 1) /!\\")
		return
	    else:
		for x in range(self.size):
		    sub.append(self.value[x] - other[x])

	return Vector(sub)


    def __rsub__(self, other):
	rsub = []

	if isinstance(other, Vector) == False:
	    if len(other) != self.size:
		print("/!\ Vectors have to be of same dimension (m * 1) /!\\")
		return
	    else:
		for x in range(self.size):
		    rsub.append(other[x] - self.value[x])
	
	return Vector(rsub)


    def __mul__(self, other):
	mul = []

	if type(other) == int or type(other) == float:
	    for x in range(self.size):
		mul.append(self.value[x] * other)
	    return Vector(mul)

	elif isinstance(other, Vector):
	    if self.size != other.size:
		print("Vectors have to be of same dimension (m * 1)")
		return
	    else:
		for x in range(self.size):
		    mul.append(self.value[x] * other.value[x])

	elif isinstance(other, Vector) == False:
	    if self.size != len(other):
		print("Vectors have to be of same dimension (m * 1)")
		return
	    else:
		for x in range(self.size):
		    mul.append(self.value[x] * other[x])
	return sum(mul)



    def __rmul__(self, other):
	rmul = []

	if type(other) == int or type(other) == float:
	    for x in range(self.size):
		rmul.append(self.value[x] * other)
	    return Vector(rmul)

	elif isinstance(other, Vector) == False:
	    if self.size != len(other):
		print("Vectors have to be of same dimension (m * 1)")
		return
	    else:
		for x in range(self.size):
		    rmul.append(self.value[x] * other[x])
	return sum(rmul)

    def __div__(self, other):
	div = []

	if type(other) == int or type(other) == float:
	    for x in range(self.size):
		div.append(self.value[x] / other)
	    return Vector(div)

	elif isinstance(other, Vector):
	    if self.size != other.size:
		print("Vectors have to be of same dimension (m * 1)")
		return
	    else:
		for x in range(self.size):
		    div.append(self.value[x] / other.value[x])

	elif isinstance(other, Vector) == False:
	    if self.size != len(other):
		print("Vectors have to be of same dimension (m * 1)")
		return
	    else:
		for x in range(self.size):
		    div.append(self.value[x] / other[x])

	return sum(div)

    def __rdiv__(self, other):
	rdiv = []

	if type(other) == int or type(other) == float:
	    print("Division of a scalar by a vector is impossible here, try the other way around.")
	    return
	
	elif isinstance(other, Vector) == False:
	    if self.size != len(other):
		print("Division of a scalar by a vector is impossible here, try the other way around.")
		return
	    else:
		for x in range(self.size):
		    rdiv.append(other[x] / self.value[x])

	return sum(rdiv)
	

    def __floordiv__(self, other):
	floordiv = []

	if type(other) == int or type(other) == float:
	    for x in range(self.size):
		floordiv.append(self.value[x] // other)
	    return Vector(floordiv)

	elif isinstance(other, Vector):
	    if self.size != other.size:
		print("Vectors have to be of same dimension (m * 1)")
		return
	    else:
		for x in range(self.size):
		    floordiv.append(self.value[x] // other.value[x])

	elif isinstance(other, Vector) == False:
	    if self.size != len(other):
		print("Vectors have to be of same dimension (m * 1)")
		return
	    else:
		for x in range(self.size):
		    floordiv.append(self.value[x] // other[x])
	
	return sum(floordiv)
    

    def __rfloordiv__(self, other):
	rfloordiv = []

	if type(other) == int or type(other) == float:
	    print("Division of a scalar by a vector is impossible here, try the other way around.")
	    return
	
	elif isinstance(other, Vector) == False:
	    if self.size != len(other):
		print("Division of a scalar by a vector is impossible here, try the other way around.")
		return
	    else:
		for x in range(self.size):
		    rfloordiv.append(other[x] // self.value[x])

	return sum(rfloordiv)
	

    def __str__(self):
	return str(self.value)

    def __repr__(self):
	return list(self.value)
