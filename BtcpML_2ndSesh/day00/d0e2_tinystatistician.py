class TinyStatistician:
    
    def mean(self, array):
    	if array == []:
	    return("/!\ Empty array, no mean available /!\\")

	else:
	    mean = 0.0;
	    for x in array:
		mean += x
	    return float(mean / len(array))


    def median(self, array):
    	if array == []:
	    return("/!\ Empty array, no quartiles available /!\\")

	else:
	    return float(array[(len(array) // 2)])

    def quartiles(self, array, p):
    	if array == []:
	    return("/!\ Empty array, no quartiles available /!\\")

	if p != 1 and  p != 3 and p != 25 and p != 75:
	    return("/!\ Second argument is the expected quartile (1 or 3) or percentile (25 or 75) /!\\")

	if p == 1 or p == 25:
	    return float(array[len(array) // 3 - 1])

	if p == 3 or p == 75:
	    return float(array[-len(array) // 3 + 1])


    def var(self, array):
    	if array == []:
	    return("/!\ Empty array, no variance available /!\\")

	else:
	    m = self.mean(array)
	    var = 0.0
	    for x in array:
		var = var + (m - x)**2
	    return float(var / len(array))


    def std(self, array):
    	if array == []:
	    return("/!\ Empty array, no standard deviation available /!\\")

	else:
	    var = self.var(array)
	    return float(var**(0.5))
