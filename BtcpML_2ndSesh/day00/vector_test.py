from d0e0_vector import Vector
from d0e2_tinystatistician import TinyStatistician
from d0e3e5_prediction import simple_predict

v1 = Vector(9)
#print(v1.size)
#print(v1.value)
v2 = Vector([0.2, 0.4, 0.6, 0.8])
#print(v2.size)
#print(v2.value)
v3 = Vector((10,5))
print(v3.size)
print(v3.value)
v4 = Vector((15,20))
print(v4.size)
print(v4.value)
v5 = [0.1, 1, 2, 3, 4, 5]
v8 = [0, 1, 2, 3, 4, 5, 6]

v10 = Vector("cinq")

v6 = v3 // 3
print(v6.value)
v7 = v3 // v5 
print(v7)
v9 = v5 // v3 
print(v9)

tstat = TinyStatistician()
a = [1, 42, 300, 10, 59]
b = [0, 15, -9, 7, 12, 3, -21] 
print("\nMEAN")
print(tstat.mean(a))
print(tstat.mean(b))
print("\nMEDIAN")
print(tstat.median(a))
print(tstat.median(b))
print("\nQUARTILES")
print(tstat.quartiles(a, 1))
print(tstat.quartiles(a, 3))
print(tstat.quartiles(b, 1))
print(tstat.quartiles(b, 3))
print("\nVARIANCE")
print(tstat.var(a))
print(tstat.var(b))
print("\nSTANDARD DEVIATION")
print(tstat.std(a))
print(tstat.std(b))



print("\nPREDICTION")
x = Vector((1,6))
theta1 = Vector([5, 0])
print(simple_predict(x.value, theta1.value))
theta2 = Vector([0, 1])
print(simple_predict(x.value, theta2.value))
theta3 = Vector([5, 3])
print(simple_predict(x.value, theta3.value))
theta4 = Vector([-3, 1])
print(simple_predict(x.value, theta4.value))

