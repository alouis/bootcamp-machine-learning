import matplotlib.pyplot as plt
import numpy as np

#MATPLOTLIB TUTO

plt.title('d01_e06 : plots')
#Print a line
x = np.linspace(0, 10, 100)
plt.plot(x, np.sin(x), color='red', linestyle='dotted', label='sin(x)')
plt.legend(loc='lower left')
#Name axes
ax = plt.axes()
ax.set(xlabel='x', ylabel='sin(x)')
#Represent noise
dy = 0.2	    #of how much can the value vary (margin of error)
y = np.sin(x) + dy *np.random.randn(100)
plt.errorbar(x, y, yerr=dy, fmt='.k')

#Print scattered points
x1 = np.arange(1,6)
y1 = np.array([3.74013816, 3.61473236, 4.57655287, 4.66793434, 5.95585554])
plt.scatter(x1, y1, color='red')

plt.show()

