import numpy as np

def add_intercept(x):
    if np.size(np.shape(x)) == 2:
	add = np.ones((np.shape(x)[0], 1))
	cnc = np.concatenate((add, x), axis=1)
	return cnc 

    elif np.size(np.shape(x)) == 1:
	add = np.ones((np.shape(x)[0],))
	result = np.c_[add, x]
	return result

    else:
	print("/!\ Paramater should be a numpy.ndarray /!\\") 
