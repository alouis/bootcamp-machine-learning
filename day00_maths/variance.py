import numpy as np

def variance(x):
	X = 0
	y = 0
	for i in range(len(x)):
		X = X + x[i]
	X = X / len(x)
	for i in range(len(x)):
		y = y + (X - x[i])**2
	y = y / len(x)
	return float(y)

X = np.array([0, 15, -9, 7, 12, 3, -21])
print(variance(X))
print(variance(X/2))
