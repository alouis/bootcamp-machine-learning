import numpy as np

def dot(x, y):
	X = 0
	if len(x) != len(y):
		return 'ERROR'
	for i in range(len(x)):
		X = X + (x[i] * y[i])
	return float(X)

X = np.array([0, 15, -9, 7, 12, 3, -21])
Y = np.array([2, 14, -13, 5, 12, 4, -19])
W = np.array([ -8, 8, -6, 14, 14, -9, -4])
Z = np.array([0, 15, -9, 7, 12, 3, -21])
#print(dot(X, Y))
#print(dot(X, X))
#print(dot(Y, Y))
print(dot(W, Z))
