import numpy as np

def mse(y, y_hat):
	W = 0
	Y = (y - y_hat)**2
	for i in range(len(Y)):
		W = W + Y[i]
	W = W / len(Y)
	return W


X = np.array([0, 15, -9, 7, 12, 3, -21])
Y = np.array([2, 14, -13, 5, 12, 4, -19])
print(mse(X, Y))
