import numpy as np

def vec_mse(y, y_hat):
	Y = sum((y - y_hat)**2) / len(y) 
	return Y

X = np.array([0, 15, -9, 7, 12, 3, -21])
Y = np.array([2, 14, -13, 5, 12, 4, -19])
print(vec_mse(X, Y))
