import numpy as np

def vec_gradient(x, y, theta):
	W = []
	for i in x:
		m = np.dot(i, theta)
		W.append(m)
	Z = 1/len(x)*np.dot(np.transpose(x), np.subtract(W, y))
	return Z

X = np.array([
	[ -6, -7, -9],
		[ 13, -2, 14],
		[ -7, 14, -1],
		[ -8, -4, 6],
		[ -5, -9, 6],
		[ 1, -5, 11],
		[ 9, -11, 8]])
Y = np.array([2, 14, -13, 5, 12, 4, -19])
Z = np.array([3,0.5,-6])
print(vec_gradient(X, Y, Z))
W = np.array([0,0,0])
print(vec_gradient(X, Y, W))
print(vec_gradient(X, X.dot(Z), Z))
