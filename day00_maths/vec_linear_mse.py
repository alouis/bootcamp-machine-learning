import numpy as np

def vec_linear_mse(x, y, theta):
	z = np.dot(x, theta)
	w = np.subtract(z, y)
	o = np.transpose(w)
	j = np.dot(w, o) / len(x)
	return j

X = np.array([
    [ -6, -7, -9],
        [ 13, -2, 14],
        [ -7, 14, -1],
        [ -8, -4, 6],
        [ -5, -9, 6],
        [ 1, -5, 11],
        [ 9, -11, 8]])
Y = np.array([2, 14, -13, 5, 12, 4, -19])
Z = np.array([3,0.5,-6])

print(vec_linear_mse(X, Y, Z))
	
