import numpy as np

def dot(x, y):
	X = 0
	for i in range(len(x)):
		X = X + (x[i] * y[i])
	return float(X)

def mean(x):
	X = 0
	for i in range(len(x)):
		X = X + x[i]
	X = X / len(x)
	return float(X)

def linear_mse(x, y, theta):
	W = []
	for i, j in zip(x, range(len(x))):
		W.append((dot(i, theta) - y[j])**2)
	return mean(W)
			
	
X = np.array([
    [ -6, -7, -9],
        [ 13, -2, 14],
        [ -7, 14, -1],
        [ -8, -4, 6],
        [ -5, -9, 6],
        [ 1, -5, 11],
        [ 9, -11, 8]])
Y = np.array([2, 14, -13, 5, 12, 4, -19])
Z = np.array([3,0.5,-6])
W = np.array([0,0,0])

print(linear_mse(X, Y, Z))
print(linear_mse(X, Y, W))
