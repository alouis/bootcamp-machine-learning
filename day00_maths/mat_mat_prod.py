import numpy as np

def dot(x, y):
    X = 0
    for i in range(len(x)):
        X = X + (x[i] * y[i])
    return X

def mat_mat_prod(x, y):
    w = []
    for i in x:
        w += [dot(i, y[:])]  # == y[:,] ++ y[:, :]
    return np.array(w)

W = np.array([
    [ -8, 8, -6, 14, 14, -9, -4],
    [ 2, -11, -2, -11, 14, -2, 14],
    [-13, -2, -5, 3, -8, -4, 13],
    [ 2, 13, -14, -15, -14, -15, 13],
    [ 2, -1, 12, 3, -7, -3, -6]])

Z = np.array([
    [ -6, -1, -8, 7, -8],
        [ 7, 4, 0, -10, -10],
        [ 7, -13, 2, 2, -11],
        [ 3, 14, 7, 7, -4],
        [ -1, -3, -8, -4, -14],
        [ 9, -14, 9, 12, -7],
        [ -9, -4, -10, -3, 6]])

print(mat_mat_prod(Z, W))
print(mat_mat_prod(W, Z))
