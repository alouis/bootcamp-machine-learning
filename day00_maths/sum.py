import numpy as np

import numpy as np

def sum_(x, f):
	X = 0
	for i in range(len(x)):
		X = X + f(x[i])
	return float(X)

X = np.array([0, 15, -9, 7, 12, 3, -21])
print(sum_(X, lambda x: x))
print(sum_(X, lambda x: x**2))
