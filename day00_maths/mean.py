import numpy as np

def mean(x):
	X = 0
	for i in range(len(x)):
		X = X + x[i]
	X = X / len(x)
	return float(X) 

X = np.array([0, 15, -9, 7, 12, 3, -21])
print(mean(X))
print(mean(X ** 2))	
