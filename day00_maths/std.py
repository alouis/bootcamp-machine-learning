import numpy as np

def std(x):
	X = 0
	y = 0
	for i in range(len(x)):
		X = X + x[i]
	X = X / len(x)
	for i in range(len(x)):
		y = y + (X - x[i])**2
	y = (y / len(x))**(1/2)
	return float(y)
	

X = np.array([0, 15, -9, 7, 12, 3, -21])
print(std(X))
Y = np.array([2, 14, -13, 5, 12, 4, -19])
print(std(Y))
